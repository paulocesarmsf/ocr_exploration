from PIL import Image
import pytesseract as ocr
from pdf2image import convert_from_path

pages = convert_from_path('../images/teste.pdf', 500)

image_counter = 1

for page in pages:
    filename = "page_" + str(image_counter) + ".jpg"
    page.save(filename, 'JPEG')
    image_counter = image_counter + 1

filelimit = image_counter-1
for i in range(1, filelimit + 1):
    filename = "page_" + str(i) + ".jpg"

    text = ocr.image_to_string(Image.open(filename))
    print(text)
