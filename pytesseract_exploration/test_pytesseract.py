from PIL import Image
import pytesseract

# https://github.com/tesseract-ocr/tesseract/wiki
# sudo apt-get install tesseract-ocr-por

# recebe o parâmetro obrigatório image, e o opcional lang, isto informa ao tesseract que o alfabeto que ele está
# tentando reconhecer é o português (suportando acentos e cedilha), caso não especificado seu padrão é ‘eng’,
# inglês.
text = pytesseract.image_to_string(Image.open('../images/phrase.jpeg'), lang='por')
print(text)

text = pytesseract.image_to_string(Image.open('../images/saoluis.jpeg'))
print(text)
