from wand.image import Image as Img
from PIL import Image
import pytesseract as ocr

with Img(filename='../images/doc_45854898.pdf', resolution=300) as img:
    img.compression_quality = 99
    img.save(filename='../images/teste_pdf.jpg')

phrase = ocr.image_to_string(Image.open('../images/teste_pdf.jpg'), lang='por')
print(phrase)
